import { Fragment } from 'react';
import { Navigate } from 'react-router-dom';
import { useEffect, useState, useContext} from 'react';

// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';
import UserContext from  "../UserContext";

export default function Courses(){

	/*
	console.log("Contents of coursesData: ");
	console.log(coursesData);
	console.log(coursesData[0]);
	*/

	const { user } = useContext(UserContext);
	const [courses, setCourses] = useState([]); // Array

	useEffect(()=>{
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setCourses(data.map(course => {
				return(
					<CourseCard key ={course._id} courseProps = {course}/>
				)
			}))
		})
	}, []);

	// Array methods to display all courses
	// let count = 0;
	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key ={course.id} courseProps = {course}/>
			)
	})*/

	/*return(
		<Fragment>
			<CourseCard courseProps={coursesData[0]} />
		</Fragment>
	)*/

	return (
			(user.isAdmin)?
				<Navigate to = "/admin" />
			:
			<>
				<h1> Courses </h1>
				{courses}
			</>
		)
}



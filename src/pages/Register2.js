import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Register() {
  const { user, setUser } = useContext(UserContext);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNumber, setMobileNumber] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');
  const [isActive, setIsActive] = useState(false);
  const [emailExists, setEmailExists] = useState(false);

  useEffect(() => {
    if (
      email !== '' &&
      password1 !== '' &&
      password2 !== '' &&
      firstName !== '' &&
      lastName !== '' &&
      mobileNumber.length >= 11 &&
      password1 === password2
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2, firstName, lastName, mobileNumber]);

  function registerUser(event) {
    event.preventDefault();

    // Check if email already exists
   

    // clear input fields
    setEmail('');
    setPassword1('');
    setPassword2('');
    setFirstName('');
    setLastName('');
    setMobileNumber('');
    setEmailExists(false);

    // Set user and redirect to login page
    setUser({ email: email });
    <Navigate to="/login" />;
  }

  // Make a POST request to register user
fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
method: 'POST',
headers: { 'Content-Type': 'application/json' },
body: JSON.stringify({
firstName,
lastName,
mobileNumber,
email,
password: password1,
}),
})
.then((response) => {
if (response.ok) {
return response.json();
} else {
throw new Error('Network response was not ok');
}
})
.then((data) => {
if (data.success) {
setUser(data.user);
Navigate('/login');
} else {
throw new Error(data.message);
}
})
.catch((error) => {
Swal.fire({
title: 'Something went wrong',
icon: 'error',
text: error.message,
});
});
}

return (
<div className="register">
<h1>Register</h1>
<Form onSubmit={registerUser}>
<Form.Group controlId="formBasicFirstName">
<Form.Label>First Name</Form.Label>
<Form.Control
type="text"
placeholder="Enter first name"
value={firstName}
onChange={(event) => setFirstName(event.target.value)}
/>
</Form.Group>
<Form.Group controlId="formBasicLastName">
<Form.Label>Last Name</Form.Label>
<Form.Control
type="text"
placeholder="Enter last name"
value={lastName}
onChange={(event) => setLastName(event.target.value)}
/>
</Form.Group>
<Form.Group controlId="formBasicMobileNumber">
<Form.Label>Mobile Number</Form.Label>
<Form.Control
type="text"
placeholder="Enter mobile number"
value={mobileNumber}
onChange={(event) => setMobileNumber(event.target.value)}
/>
</Form.Group>
<Form.Group controlId="formBasicEmail">
<Form.Label>Email address</Form.Label>
<Form.Control
type="email"
placeholder="Enter email"
value={email}
onChange={(event) => setEmail(event.target.value)}
/>
</Form.Group>
{emailExists && (
<p style={{ color: 'red' }}>This email is already registered</p>
)}
<Form.Group controlId="formBasicPassword1">
<Form.Label>Password</Form.Label>
<Form.Control
type="password"
placeholder="Password"
value={password1}
onChange={(event) => setPassword1(event.target.value)}
/>
</Form.Group>
<Form.Group controlId="formBasicPassword2">
<Form.Label>Confirm Password</Form.Label>
<Form.Control
type="password"
placeholder="Confirm Password"
value={password2}
onChange={(event) => setPassword2(event.target.value)}
/>
</Form.Group>
<Button
       variant="primary"
       type="submit"
       disabled={!isActive}
       className="btn-register"
     >
Register
</Button>
</Form>
</div>
);
}